<?php

/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/01/2017
 * Time: 14:47
 */

namespace giftbox\utils;

use acurrieclark\PhpPasswordVerifier\Verifier;
use giftbox\controleurs\controllerUtilisateur;
use giftbox\models\prestation;
use giftbox\models\utilisateur;
use giftbox\vues\VueAccueil;
use giftbox\vues\VueCatalogue;
use giftbox\vues\VueFormCoffret;
use acurrieclark\PhpPasswordVerifier\Pusher;

class authentification
{

    private $user;

    public static function createUser ($userName, $password) {
        //si le nom d'utilisateur est disponible
        if(utilisateur::where('username', '=',$userName)->count()==0) {
            //verifier = police de mot passe
            $verifier = new Verifier();
            //on veut un mdp de 8 à 40 caractères, avec au moins 1 majuscule, 1 nombre et 1 caractère spécial
            $verifier->setMinLength(8);
            $verifier->setMaxLength(40);
            $verifier->setCheckContainsLetters(true);
            $verifier->setCheckContainsNumbers(true);
            $verifier->setCheckContainsCapitals(true);
            $verifier->setCheckContainsSpecialChrs(true);
            //on vérifie si le password se plie à la police
            $result = $verifier->checkPassword($password);
            //si il est valide
            if ($result) {
                //cryptage
                $hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
                //création de l'utilisateur
                $util = new utilisateur();
                $util->userName = $userName;
                $util->password = $hash;
                $util->save();
                //retour à la page d'Accueil
                $prestation = prestation::get();
                $vue = new VueAccueil();
                echo $vue->render();
            } else {
                //si password non conforme, message d'erreur
                $vue = new VueFormCoffret('Le Password n\'est pas valide');
                echo $vue->render(2);
            }
        } else {
            $vue = new VueFormCoffret('Ce nom d\'utilisateur est déjà prit');
            echo $vue->render(2);
        }

    }

    public static function authenticate ($username, $password) {
        //si le username est bien dans la table
        if(utilisateur::where('username', '=', $username)->count() == 1) {
            //récupération de l'utilisateur
            $util = utilisateur::where('username', '=', $username)->first();
            //si mdp ok
            if (password_verify($password, $util->password)) {
                //on charge l'utilisateur
                self::loadProfile($util);
            } else {
                //affichage d'erreur
                $vue = new VueFormCoffret('Erreur d\'authentification');
                echo $vue->render(2);
            }
        } else {
            //affichage d'erreur
            $vue = new VueFormCoffret('L\'utilisateur n\'existe pas dans la base');
            echo $vue->render(2);
        }
    }

    private static function loadProfile ($user) {
        //On supprime la valeur de 'user' au cas ou
        unset($_SESSION['user']);
        //on y met le nouveau username
        $_SESSION['user'] = $user->userName;
        //affichage de l'Accueil
        $prestation = prestation::get();
        $vue = new VueAccueil();
        echo $vue->render();
    }

    public static function checkAccessRights ($required) {
        //inutil ici puisqu'il n'y a qu'un niveau d'authentification
        if(array_key_exists('user', $_SESSION)){
            if($_SESSION['user']->droit >= $required)
                return true;
            else
                return false;
        }
        else
            return false;
    }


}