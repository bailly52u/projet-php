<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/12/2016
 * Time: 17:15
 */

namespace giftbox\controleurs;

require 'vendor/autoload.php';

use giftbox\models\categorie as categorie;
use giftbox\models\coffret;
use giftbox\models\note;
use giftbox\models\prestation as prestation;
use giftbox\vues\VueCatalogue as VueCatalogue;
use giftbox\vues\VueFormCoffret;
use Slim\Slim;

class catalogue
{
    public function listerPrestations()
    {
        //on récupère toute les prestations
        $prestation = prestation::where('active','=',1)->get();
        //on les passe à la vue pour les afficher
        $vue = new VueCatalogue($prestation);
        echo $vue->render(1);
    }

    public function listerPrestationsAdmin()
    {
        //on récupère toute les prestations
        $prestation = prestation::get();
        //on les passe à la vue pour les afficher
        $vue = new VueCatalogue($prestation);
        echo $vue->render(1);
    }


    public function listerCategories () {
        //on récupère toute les catégories
        $categorie = categorie::get();
        //on les passe à la vue pour les afficher
        $vue = new VueCatalogue($categorie);
        echo  $vue->render(2);
    }

    public function donnerPrestation($id)
    {
        //On cherche la prestation à l'id passé en paramètre
        $p = prestation::where('id', '=', $id)->first();
        //récupération des notes associées
        $notation = $p->note;
        $moyenne = 0;
        $div = 0; //nb Total de notes
        //Si il y a bien des notes
        if(sizeof($notation)!=0) {
            //calcul de la moyenne en parcourant les notes
            foreach ($notation as $n) {
                $div += $n->pivot->quantite;
                $moyenne += $n->valeur * $n->pivot->quantite;
            }
            $moyenne = round($moyenne / $div, 1);
        }
        //envoie de la prestation et moyenne à la vue
        $listeObjet['prestation'] = $p;
        $listeObjet['moyenne'] = $moyenne;
        $vue = new VueCatalogue($listeObjet);
        echo $vue->render(3);
    }

    public function donnerPrestationsCategorie($id)
    {
        //On cherche la catégorie
        $p = categorie::where('id', '=', $id)->first();
        //on l'envoie à la vue, qui en extraira les différentes prestation pour affichage
        $vue = new VueCatalogue($p);
        echo $vue->render(4);
    }

    public function trierPrix($sens)
    {
        //si le sens passé en paramètre est valide
        if (in_array($sens, array('ASC', 'DESC'))) {
            //on tri dans ce sens
            $posts = prestation::orderBy('prix', $sens)->get();
        } else {
            //sinon, on tri par ordre croissant (défaut)
            $posts = prestation::orderBy('prix', 'ASC')->get();
        }
        //on envoie les prestations triées à la vue qui les affiches
        $vue = new VueCatalogue($posts);
        echo $vue->render(1);
    }

    public function noterPrestation($id, $note) {
        //on cherche la prestation à noter
        $prestation = prestation::find($id);
        //on récupère sa liste de notes
        $notation = $prestation->note;
        //boolean permettant de savoir s'il faut créer une ligne ou si l'update a été effectué
        $ajout = false;
        //on parcourt les notes
        foreach ($notation as $n) {
            //si c'est la note demandée
            if ($n->idNote == $note) {
                //récupération de la quantité + maj
                $qtt = $n->pivot->quantite+1;
                //update du Pivot
                $prestation->note()->updateExistingPivot($note, ['quantite' => $qtt]);
                //on indique que l'update est effectué
                $ajout = true;
                //on stop la boucle
                break;
            }
        }
        //Dans le cas ou la note n'est pas trouvée, il faut l'ajouter
        if(!$ajout)
            //ajout de la note dans la table pivot pour la prestation voulu à une quantité de 1
            $prestation->note()->attach($note, ['quantite' => 1]);
    }

    public function formAjoutPrestation() {
        $vue = new VueCatalogue('');
        echo $vue->render(5);
    }

    public function ajouterPrestation($infos){
        $app = Slim::getInstance();
        $prestation = new prestation();
        $prestation->nom = $infos['nom'];
        $prestation->descr = $infos['descr'];
        $prestation->cat_id = $infos['cat'];
        $prestation->prix = $infos['prix'];
        $chemin = $_FILES['image']['name'];
        $prestation->img = $chemin;
        //A modifier sinon c'est la mort
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $resultat = move_uploaded_file($_FILES['image']['tmp_name'], 'img/'.$chemin);
        if($resultat){
            $prestation->save();
        } else {
            $vue = new VueFormCoffret('L\image ne peut pas être sauvée');
            echo $vue->render(2);
        }
        return $resultat;
    }

    public function supprPrestation($id_prest){
        $prestation = prestation::where('id','=',$id_prest)->first();

        if($prestation->delete())
            return 1;
        else
            return 0;

    }

    public function desactiverPrestation($id_prest){
        $prestation = prestation::where('id','=',$id_prest)->first();
        $prestation->active=0;
        $prestation->save();
    }

    public function activerPrestation($id_prest){
        $prestation = prestation::where('id','=',$id_prest)->first();
        $prestation->active=1;
        $prestation->save();
    }



}