<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/01/2017
 * Time: 22:24
 */

namespace giftbox\controleurs;

use giftbox\vues\VueAuthentification;
use giftbox\utils\authentification;

class controllerUtilisateur
{
    public function envoyerFormulaireInscription(){
        //on affiche le formulaire d'inscription
        $vue = new VueAuthentification();
        echo $vue->render(1);
    }

    public function verifierFormulaireInscription($infos)
    {
        //inscription de l'utilisateur après vérification
        authentification::createUser($infos['id'], $infos['passwd']);
    }

    public function envoyerFormulaireConnexion(){
        //on affiche le formulaire de connexion
        $vue = new VueAuthentification();
        echo $vue->render(2);
    }

    public function verifierFormulaireConnexion($infos)
    {
        //authentification de l'utilisateur avec ID et MDP
        authentification::authenticate($infos['id'], $infos['passwd']);
    }

    public function deconnecterUtilisateur(){
        //suppression de la variable user dans la session
        unset($_SESSION['user']);
    }
}