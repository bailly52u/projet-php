<?php
/**
 * Created by PhpStorm.
 * User: Romain KRAFT
 * Date: 20/12/2016
 * Time: 11:05
 */

namespace giftbox\controleurs;


use giftbox\models\coffret;
use giftbox\models\prestation;
use giftbox\vues\VueCadeau;
use giftbox\vues\VueCagnotte;
use giftbox\vues\VueCoffret;
use giftbox\vues\VueFormCoffret;
use Slim\Slim;

class controllerCoffret
{

    public function suiviCoffret($url)
    {
        //on cherche le coffret correspondant à l'URL gestion passé en paramètre
        $co = coffret::where('url_gestion', '=', $url)->first();
        //Si le coffret ne demande pas de mot de passe, ou qu'il est déjà dans la session
        if (!isset($co->password) || (array_key_exists('auth', $_SESSION) && array_key_exists($co->url_gestion, $_SESSION['auth']) && $_SESSION['auth'][$co->url_gestion])) {
            //La vue affiche l'interface de suivi
            $vue = new VueCoffret($co);
            echo $vue->render(2);
        } else {
            //sinon, on demande à la vue de le demander
            $vue = new VueCoffret(null);
            echo $vue->render(7);
        }

    }
//
    public function login($url)
    {
        //si un password est donné
        if(array_key_exists('password',$_POST))
        {
            //on récupère le coffret lié à l'url gestion
            $co = coffret::where('url_gestion','=',$url)->first();
            //si le password est bon
            if (password_verify($_POST['password'], $co->password)) {
                //on l'indique dans la session (retour à la fonction suiviCoffret)
                $_SESSION['auth'][$co->url_gestion] = true;
                $this->suiviCoffret($url);
            }else
            {
                //sinon, on affiche une erreur
                $cad = new VueFormCoffret("Mot de passe invalide !");
                echo $cad->render(2);
            }
        }else
        //si il n'y a pas de mot de passe, on envoie une erreur
        {
            $cad = new VueFormCoffret("Mot de passe invalide !");
            echo $cad->render(2);
        }
    }

    public function afficherMonCoffret()
    {
        $prestation = null;
        //Si il y a des prestations dans le coffret, on les récupères
        if (array_key_exists('prestation', $_SESSION)) {
            $prestation = $_SESSION['prestation'];
        }
        //Affichage du coffret (peut être vide)
        $coffret = new VueCoffret($prestation);
        echo $coffret->render(1);
    }

    public function ajouterPrestation($id, $quantite)
    {
        $prestation = prestation::where('id', '=', $id)->first();
        //On vérifie que la prestation est présente dans la BDD
        if (isset($prestation)) {

            //On vérifie que la quantité est valide
            if (is_numeric($quantite) && (int)$quantite > 0) {
                //Si la prestation n'est pas dans la variable de session on le créé
                if (!isset($_SESSION['prestation'])) {
                    $_SESSION['prestation'] = array();
                }

                //Si l'objet est deja dans le panier on change sa quantité sinon on ajoute l'objet et la quantité dans le panier
                if (key_exists($prestation->id, $_SESSION['prestation'])) {
                    $_SESSION['prestation'][$prestation->id]['quantite'] = $quantite;
                } else {
                    $_SESSION['prestation'][$prestation->id] = array('object' => $prestation,
                        'quantite' => $quantite);
                }
            } elseif ((int)$quantite == 0) {
                $this->supprimerPrestation($id);
            } else {
                die('Erreur la quantite n\'est pas valide.');
                return 1;
            }
        } else {
            die('La prestation n\'existe pas.');
            return 1;
        }
    }

    public function supprimerPrestation($id)
    {
        //si on a des prestations dans le coffret
        if (key_exists($id, $_SESSION['prestation'])) {
            //on supprime la prestation voulu de la session
            unset($_SESSION['prestation'][$id]);
        }
    }

    public function preparerEnvois()
    {
        if($_SESSION['indiceFormulaire']!=4) {
            $id = $_SESSION['coffret_id'];
            $co = coffret::where('coffret_id', '=', $id)->first();
            $co->paye = 1;
            $co->save();
        }
        $vue = new VueCoffret(15);
        echo $vue->render(5);
    }

    public function genererURL(){
        $app = Slim::getInstance();
        $mail=$_POST['mail'];
        $id=$_SESSION['coffret_id'];
        $co=coffret::where('coffret_id','=',$id)->first();
        $sender=$co->mail_crea;
        $URL=bin2hex(openssl_random_pseudo_bytes(16));
        $co->url_cadeau=$URL;
        if($_SESSION['indiceFormulaire']!=4) {
            $co->mode_ouverture=$_POST['typeOuverture'];
            $date_ouverture=time();
            if (isset($_POST['date_ouverture'])) {
                $date_ouverture=$_POST['date_ouverture'];
            }
            $co->date_ouverture=$date_ouverture;
            $co->status=3;
            $co->save();
            $passage_ligne = "\r\n";
            //Déclaration des messages au format HTML.
            $message_html = "<html><head></head><body>" . $co->message . "<br>Et voici le lien qui permet de recuperer ce cadeau <a href='http://".$_SERVER['HTTP_HOST']. $app->urlFor('cadeau',['url_cadeau'=>$URL]). "'>ici</a></body></html>";
            //Création de la boundary
            $boundary = "-----=" . md5(rand());
            //Définition du sujet.
            $sujet = "CADEAU !";
            //Création du header de l'e-mail.
            $header = "From: \"$sender\"<$sender>" . $passage_ligne;
            $header .= "Reply-to: \"$sender\" <$sender>" . $passage_ligne;
            $header .= "MIME-Version: 1.0" . $passage_ligne;
            $header .= "Content-Type: multipart/alternative;" . $passage_ligne . " boundary=\"$boundary\"" . $passage_ligne;
            //Création du message.
            $message = $passage_ligne . "--" . $boundary . $passage_ligne;
            //Ajout du message au format HTML
            $message .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $passage_ligne;
            $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
            $message .= $passage_ligne . $message_html . $passage_ligne;
            //Envoi de l'e-mail.
            mail($mail,$sujet,$message,$header);
        }
        $vue = new VueCoffret($co);
        echo $vue->render(6);
        unset($_SESSION['prestation']);
        unset($_SESSION['infos']);
    }

    public function envoisCadeauCagnotte(){
        $app = Slim::getInstance();
        $id=$_POST['coffret_id'];
        $co=coffret::where('coffret_id','=',$id)->first();
        $date_ouverture=time();
        if (isset($_POST['date_ouverture'])) {
            $date_ouverture=$_POST['date_ouverture'];
        }
        //$co->date_ouverture=$date_ouverture;
        $sender=$co->mail_crea;
        $URL=bin2hex(openssl_random_pseudo_bytes(16));
        $co->url_cadeau=$URL;
        $mail=$_POST['mail'];

        $passage_ligne = "\r\n";
        //Déclaration des messages au format HTML.
        $message_html = "<html><head></head><body>" . $co->message . "<br>Et voici le lien qui permet de recuperer ce cadeau <a href='http://".$_SERVER['HTTP_HOST']. $app->urlFor('cadeau',['url_cadeau'=>$URL]). "'>ici</a></body></html>";
        //Création de la boundary
        $boundary = "-----=" . md5(rand());
        //Définition du sujet.
        $sujet = "CADEAU !";
        //Création du header de l'e-mail.
        $header = "From: \"$sender\"<$sender>" . $passage_ligne;
        $header .= "Reply-to: \"$sender\" <$sender>" . $passage_ligne;
        $header .= "MIME-Version: 1.0" . $passage_ligne;
        $header .= "Content-Type: multipart/alternative;" . $passage_ligne . " boundary=\"$boundary\"" . $passage_ligne;
        //Création du message.
        $message = $passage_ligne . "--" . $boundary . $passage_ligne;
        //Ajout du message au format HTML
        $message .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $passage_ligne;
        $message .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $message .= $passage_ligne . $message_html . $passage_ligne;
        //Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);

        $vue=new VueCoffret($co);
        echo $vue->render(6);
    }
    
    public function afficherCoffret($url)
    {
        $co = coffret::where('url_cadeau','=',$url)->first();
        $vue = new VueCoffret($co);
        echo $vue->render(2);

    }

    public function afficherPaiementCoffret(){
        $prestation = null;
        $infos = null;
        if (array_key_exists('prestation', $_SESSION) && $_SESSION['indiceFormulaire']==3) {
            $prestation = $_SESSION['prestation'];
            $infos = $_SESSION['infos'];
            $coffretBDD = new coffret();
            $coffretBDD->nom_crea = $infos['nom'];
            $coffretBDD->prenom_crea = $infos['prenom'];
            $coffretBDD->mail_crea = $infos['mail'];
            $coffretBDD->message = $infos['msg'];
            $URL=bin2hex(openssl_random_pseudo_bytes(16));
            $coffretBDD->url_gestion = $URL;
            $coffretBDD->password=$infos['password'];
            $coffretBDD->save();
            $_SESSION['coffret_id']=$coffretBDD->coffret_id;
            if(array_key_exists('paiement',$infos) && $infos['paiement']!=='cagnotte') {

                foreach ($prestation as $prest) {
                    $coffretBDD->prestation()->attach($prest['object']->id, ['quantite' => $prest['quantite']]);
                }

                $_SESSION['infos'] = NULL;
                $coffret = new VueCoffret($prestation);
                echo $coffret->render(3);

            }else{

                $prix = 0;
                foreach ($prestation as $prest) {
                    $coffretBDD->prestation()->attach($prest['object']->id, ['quantite' => $prest['quantite']]);
                    $prix+=$prest['object']->prix*$prest['quantite'];
                }

                $coffretBDD->mode_paiement=1;
                $coffretBDD->prix_Total = $prix;
                $coffretBDD->somme_atteinte=0;
                $URL=bin2hex(openssl_random_pseudo_bytes(16));
                $coffretBDD->url_cagnotte = $URL;
                $coffretBDD->save();
                $_SESSION['indiceFormulaire']=4;
                $_SESSION['infos'] = NULL;

                $coffret = new VueCoffret($prestation);
                $this->preparerEnvois();
            }
        }
        else {
            $coffret = new VueFormCoffret("Vous n'êtes pas autorisé à accéder à cette page");
            echo $coffret->render(2);
        }
    }

    public function payerCoffret(){
        if (isset($_POST['numCarte']) && isset($_POST['cryp'])){
            $num=$_POST['numCarte'];
            $cryp=$_POST['cryp'];
            if (strlen($num)==16){
                if (strlen($cryp)==3){
                    $id=$_SESSION['coffret_id'];
                    $co = coffret::where('coffret_id','=',$id)->first();
                    $co->paye=1;
                    $co->save();
                }else{
                    $vueFormCo=new VueFormCoffret('cryptogramme incorrect');
                    $vueFormCo->render(2);
            }
            }else{
                $vueFormCo=new VueFormCoffret('numéro de carte invalide');
                $vueFormCo->render(2);
            }
        }
    }

    public function verifierCoffret(){
        //Vérification de l'existance de prestations
        if (isset($_SESSION['prestation'])) {
            $nbPrest = count($_SESSION['prestation']);
            //Vérification au moins 2 presta différentes
            if ($nbPrest >= 2){
                $cpt = 0;
                //Recherche de deux prestations de catégories différentes
                foreach($_SESSION['prestation'] as $value){
                    $cat1 = $value['object']['cat_id'];
                    foreach ($_SESSION['prestation'] as $value2){
                        //Si on a deux catégories différentes
                        if($cat1 != $value2['object']['cat_id'])
                            $cpt ++;
                        if($cpt>=2)
                            break;
                    }
                    if($cpt>=2)
                        break;
                }
                //Si on a deux catégories différentes
                if($cpt>=2){
                    $_SESSION['indiceFormulaire'] = 2;
                    $formCoffret = new VueFormCoffret();
                    echo $formCoffret->render(1);
                }
                //Si une seule catégorie
                else {
                    $formCoffret = new VueFormCoffret("Il faut au moins 2 catégories différentes");
                    echo $formCoffret->render(2);
                }
            }
            //Si une seule prestation
            else {
                $formCoffret = new VueFormCoffret("Il faut au moins 2 prestations");
                echo $formCoffret->render(2);
            }
        }
        //Si aucune presation
        else {
            $formCoffret = new VueFormCoffret("Vous n'avez pas ajouté de prestations au coffret");
            echo $formCoffret->render(2);
        }
    }

    public function afficherCadeaux($url_cadeau){
        $coffret = coffret::where('url_cadeau','=',$url_cadeau)->first();
        $prestation = $coffret->prestation();

        if($coffret->mode_ouverture == 1) {
            $cad = new VueCadeau($prestation,$url_cadeau);
            echo $cad->render(1);
        }elseif ($coffret->mode_ouverture == 2) {
            $timestamp = time();
            $date_ouverture = strtotime($coffret->date_ouverture);

            if($date_ouverture <= $timestamp) {
                $cad = new VueCadeau($coffret);
                echo $cad->render(2);
            }
            else {
                $cad = new VueFormCoffret($coffret->date_ouverture);
                echo $cad->render(3);
            }
        }else{
            $vue = new VueFormCoffret("Aucun coffret trouvé");
            echo $vue->render(2);
        }
    }

    public function validerCoffret(){
        $infos = $_SESSION['infos'];
        //test que la session existe et qu'on est autorisé à aller sur cette page
        if(isset($_SESSION['indiceFormulaire']) && $_SESSION['indiceFormulaire'] == 2){
            //test que les champs sont remplis
            if(isset($_POST['mail']) && isset($_POST['nom']) && isset($_POST['prenom']))
            {
                //test la validité de l'email
                if (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                    if(array_key_exists('prestation',$_SESSION)) //Inutile puisque si on arrive sur cette page sans presta (illégalement) on a une erreur
                    {
                        if(array_key_exists('password',$_POST)&& $_POST['password']!='')
                        {
                            $hash=password_hash($_POST['password'], PASSWORD_DEFAULT ,['cost'=> 12] );
                            $_SESSION['infos']['password'] = $hash;
                        }else{
                            $_SESSION['infos']['password'] = null;
                        }

                        $_SESSION['indiceFormulaire'] = 3;
                        $vue = new VueCoffret($_SESSION['prestation']);
                        echo $vue->render(4);
                    }
                    //si pas de presta (encore une fois impossible)
                    else{
                        $vue = new VueFormCoffret("Une erreur s'est produite");
                        echo $vue->render(2);
                    }
                //si adresse mail non valide
                }else{
                    $vue = new VueFormCoffret("L'adresse email n'est pas valide");
                    echo $vue->render(2);
                }
            }
            //si champs non remplis
            else{
                $vue = new VueFormCoffret("Vous n'êtes pas autorisé à vous rendre sur cette page");
                echo $vue->render(2);
            }
        }
    }

    public function afficherCagnotte($url){
        $coffret = coffret::where('url_cagnotte', '=', $url)->first();
        $infos['prix_total'] = $coffret->prix_total;
        $infos['somme_atteinte'] = $coffret->somme_atteinte;
        $vue = new VueCagnotte($coffret, $infos);
        echo $vue->render(1);
    }

    public function enregistrerParticipation(){
        $montant=$_POST['montant'];
        $url_cagnotte=$_POST['url_cagnotte'];

        $coffret=coffret::where('url_cagnotte','=',$url_cagnotte)->first();
        $coffret->somme_atteinte=$coffret->somme_atteinte+$montant;
        if (isset($_POST['message'])) {
            $coffret->message=$coffret->message."<br>".$_POST['message'];
        }
        $coffret->save();

        $app = \Slim\Slim::getInstance();
        $app->response->redirect($app->urlFor('cagnotte', ['url_cagnotte'=>$url_cagnotte]));
    }

}
