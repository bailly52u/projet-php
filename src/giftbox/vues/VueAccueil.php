<?php

namespace giftbox\vues;

use giftbox\models\categorie;
use giftbox\models\prestation;

class VueAccueil
{
	private $content;

    public function render()
    {
        $this->content = $this->afficherAccueil();

        $html = Vue::render($this->content);
        echo $html;
    }

    private function afficherAccueil()
    {
    	$content="<h1>Meilleure prestation de chaque catégorie</h1>";
    	$listeCategorie = categorie::get();
    	foreach ($listeCategorie as $categorie) {
    		$catId=$categorie->id;
    		$listePrestaCat=prestation::where('cat_id','=',$catId)->where('active','=',1)->get();
    		$moyMax=0;
    		unset($prestaNoteMax);
    		foreach ($listePrestaCat as $prestation) {
		        //récupération des notes associées
        		$notation = $prestation->note;
        		$moyenne = 0;
        		$div = 0; 
        		//Si il y a bien des notes
        		if(sizeof($notation)!=0) {
        		    //calcul de la moyenne en parcourant les notes
        		    foreach ($notation as $n) {
        		        $div += $n->pivot->quantite;
        		        $moyenne += $n->valeur * $n->pivot->quantite;
        		    }
        		    $moyenne = round($moyenne / $div, 1);
        		}
        		if ($moyenne>$moyMax) {
        			$prestaNoteMax=$prestation;
        			$moyMax=$moyenne;
        		}
        		
    		}
    		if (isset($prestaNoteMax)) {
                $app = \Slim\Slim::getInstance();
                $categorieLink = $app->urlFor('categorie',['id'=>$categorie->id]);
                $rootUri = $app->request->getRootUri();
                $prestationLink = $app->urlFor('prestation',['id'=>$prestaNoteMax->id]);
    				$content .= <<<END
            	<div class="prestation list">
            	   <p>
            	   <h2>Catégorie : <a href='$categorieLink'>$categorie->nom</a></h2>
            	   <h4><a href="$prestationLink" >$prestaNoteMax->nom</a></h4>$prestaNoteMax->prix €
            	   </p>
            	   <img src='${rootUri}/img/$prestaNoteMax->img' alt='$prestaNoteMax->nom'>
            	</div>
END;
    			}
    	}
    	return $content;
    }
}