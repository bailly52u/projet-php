<?php
/**
 * Created by PhpStorm.
 * User: Romain KRAFT
 * Date: 19/12/2016
 * Time: 10:28
 */

namespace giftbox\vues;


class Vue
{

    static function render($content)
    {
        $app = \Slim\Slim::getInstance();
        $acceuil = $app->urlFor('root');
        $lPrestation = $app->urlFor('lPrestation');
        $lCategorie = $app->urlFor('lCategorie');
        $inscription = $app->urlFor('inscription');
        $deconnexion = $app->urlFor('deconnexion');
        $monCoffret = $app->urlFor('coffret');
        $connexion = $app->urlFor('connexion');
        $ajoutPrestation = $app->urlFor('formAjoutPrestation');
        $html = <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset='utf-8'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="${acceuil}/include/css/stylesheet.css" />
            <title>Projet-PHP</title>
            <link rel="shortcut icon" href="${acceuil}/img/icon/icon.png" />
        </head>
        <body>
        <div id="navbar" class="navbar navbar-static-top fixed-nav-bar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="$acceuil">Accueil</a></li>
            <li><a href="$lPrestation">Prestations</a></li>
            <li><a href="$lCategorie">Catégories</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
END;
        if(array_key_exists('user', $_SESSION)){
            $html .= "<li><a href=\"$ajoutPrestation\">Ajouter une prestation</a></li>";
            $html .= "<li><a href=\"$inscription\">Créer Gestionnaire</a></li>";
            $html .= "<li><a href=\"$deconnexion\">Déconnexion</a></li>";
            $html .= "<li><a href=\"$monCoffret\">".$_SESSION['user']."</a></li>";
        }else {
            $html .= "<li><a href=\"$connexion\">Connexion</a></li><li><a href=\"$monCoffret\">Mon Coffret</a></li>";
        }

        $html .= <<<END
          </ul>
        </div>
        <div class="container">
            $content
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        </body></html>
END;
        return $html;
    }

}