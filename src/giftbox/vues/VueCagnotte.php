<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 10/01/2017
 * Time: 11:00
 */

namespace giftbox\vues;


use giftbox\models\coffret;
use Slim\Slim;

class VueCagnotte
{
    protected $content, $listeObjet, $infosCagnotte;

    public function __construct($liste, $infos)
    {
        $this->listeObjet = $liste;
        $this->infosCagnotte = $infos;
    }

    public function render($typeVue)
    {
        switch ($typeVue) {
            case 1 :
                $this->afficherCagnotte();
                break;
        }
        $html = Vue::render($this->content);
        return $html;
    }

    public function afficherCagnotte()
    {
        $app = Slim::getInstance();
        $ajouterPanier = $app->urlFor('ajouterPrestationPanier');
        $root = $app->urlFor('root');

        $this->content .= "<h1>Cagnotte :</h1><div class ='panier'>";
        if (isset($this->listeObjet)) {
            foreach ($this->listeObjet->prestation as $p) {
                $url_cagnotte=$this->listeObjet->url_cagnotte;
                $prestation = $p;
                $quantite = $p->pivot->quantite;
                $prix = $prestation->prix * $quantite;
                $this->content .= <<<END
                <div class='prestation'>
                    <div class='pic col-md-3'>
                        <img src='${root}/img/$prestation->img' alt='$prestation->nom'>
                    </div>
                    <div class='info col-md-6 contenu '>
                             <h3>$prestation->nom</h3>
                             <p>$prestation->descr</p>
                             <p>$prestation->prix €</p>
                     </div>
                    <div class='montant'>
                        <h4>Sous Total : $prix €</h4>
                        <form action='${ajouterPanier}' method='POST'>
                            <label for='qtt'>Quantité</label>
                            <label for='qtt'>$quantite</label>
                        </form>
                    </div>
                </div>
END;
            }

            $prix_total = $this->infosCagnotte['prix_total'];
            $somme_atteinte = $this->infosCagnotte['somme_atteinte'];
            $enregistrer = $app->urlFor('enregistrerParticipation');
            $this->content .= <<<END
            <br/>
        <div class='montant'>
                    <h4>Cout total : $prix_total  €</h4>
                    <h4>Déjà réglé : $somme_atteinte €</h4>
                     <br/>
                    <form action='${enregistrer}' method='POST'>
                    
                        <label for='numCarte'>Numéro de carte : </label>
                        <input type='number' name='numCarte' id='numCarte' placeholder='0000-0000-0000-0000' required>
                        <br>
                        <label for='date'>Date de validité : </label>
                        <select name="mois" id='date'>
                            <option>01 - Janvier</option>
                            <option>02 - Février</option>
                            <option>03 - Mars</option>
                            <option>04 - Avril</option>
                            <option>05 - Mai</option>
                            <option>06 - Juin</option>
                            <option>07 - Juillet</option>
                            <option>08 - Août</option>
                            <option>09 - Septembre</option>
                            <option>10 - Octobre</option>
                            <option>11 - Novembre</option>
                            <option>12 - Décembre</option>
                        </select>
                        <select name="annee">
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                            <option>2020</option>
                            <option>2021</option>
                            <option>2022</option>
                            <option>2023</option>
                            <option>2024</option>
                            <option>2025</option>
                            <option>2026</option>
                            <option>2027</option>
                            <option>2028</option>
                        </select>
                        <br>
                        <label for='cryp'>Cryptogramme : </label>
                        <input type='number' value='Cryptogramme' name='cryp' required>
                        <br>
                        <label for='montant'>Montant : </label>
                        <input type='number' value='montant' name='montant' required>
                        <br>
                        <label for='message'>Message : </label><br>
                        <textarea name='message' id='message'></textarea><br>
                        <br>
                        <input type='hidden' name='url_cagnotte' value="$url_cagnotte" > 
                        <input type='submit' value='Valider' class='btn btn-primary'>
                    </form>
                </div>
END;

        }
    }
}