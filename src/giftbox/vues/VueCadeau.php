<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/01/2017
 * Time: 15:46
 */

namespace giftbox\vues;

use giftbox\models\coffret;
use giftbox\models\prestation;
use Slim\Slim;


class VueCadeau
{
    protected $content, $listeObjet, $url_cadeau;

    public function __construct($listeObjet, $url_cadeau = null)
    {
        $this->listeObjet = $listeObjet;
        $this->url_cadeau = $url_cadeau;
    }

    public function render($typeVue)
    {
        $app = Slim::getInstance();
        $cadeau = $app->urlFor('cadeau',['url'=>$this->url_cadeau]);
        switch ($typeVue) {
            case 1 :
                $this->content = $this->afficherListePasAPas();
                header('Location : '.$cadeau);
                break;
            case 2 :
                $this->content = $this->afficherListePrestationCadeauAll($this->listeObjet);
                break;
        }

        $html = Vue::render($this->content);
        return $html;
    }

    private function afficherListePasAPas()
    {
        $coffret = coffret::where('url_cadeau', '=', $this->url_cadeau)->first();

        $liste_prestations = array();

        foreach ($coffret->prestation as $presta) {
            $id_prestation = $presta->id;
            $liste_prestations[] = $id_prestation;
        }

        $max = count($liste_prestations);

        if ($coffret->status == 2)
            return $this->afficherListePrestationCadeauAll($coffret);

        if (!isset($_SESSION['prestCourante']) || $coffret->status == 3) {
            $_SESSION['prestCourante'] = 0;
            $coffret->status = 1;
            $coffret->save();
        }
        else {
            if ($_SESSION['prestCourante'] < $max - 1 && $_SESSION['prestCourante'] >= 0) {
                $_SESSION['prestCourante']++;
            } else {
                unset($_SESSION['prestCourante']);
                $coffret->status = 2;
                $coffret->save();
                return $this->afficherListePrestationCadeauAll($coffret);
            }
        }
        $app = Slim::getInstance();
        $prestation = $app->urlFor('prestation');
        $root = $app->urlFor('root');
        $p = $liste_prestations[$_SESSION['prestCourante']];
        $presta = prestation::where('id', '=', $p)->first();
        $content = <<<END
            <div class="prestation list">
                <h4><a href="$prestation/$p" >$presta->nom</a></h4>
                 <img src='${root}/img/$presta->img' alt='$presta->nom'>
            </div>
            <form method='get'>
                <input type='submit' class='btn btn-primary' value='Suivant'>
            </form>

END;

        return $content;
    }

    private function afficherListePrestationCadeauAll($coffret)
    {
        $app = Slim::getInstance();
        $prestation = $app->urlFor('prestation');
        $root = $app->urlFor('root');
        $coffret->status=2;
        $coffret->save();
        $content = "<h1>Cadeau :</h1><div class ='panier'>";
        //On affiche toutes les prestations du coffret
        foreach ($coffret->prestation as $presta) {
            $quant = $presta->pivot->quantite;
            $content .= <<<END
            <div class='prestation'>
                <div class='col-md-3'>
                    <img src='${root}/img/$presta->img'  alt='$presta->nom'>
                </div>
                <div class='info col-md-6 contenu '>
                    <h3>$presta->nom</h3>
                    <p>$presta->descr</p>
                    <p>$quant</p>
                </div>
            </div>
        
END;
        }
        return $content;
    }
}