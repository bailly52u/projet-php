<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 03/01/2017
 * Time: 17:13
 */

namespace giftbox\vues;
use Slim\Slim;
class VueFormCoffret
{

    private $content, $message;

    public function __construct($msg = null)
    {
        $this->message = $msg;
    }

    public function render($typeVue)
    {
        switch ($typeVue)
        {
            case 1 :
                $this->afficherFormulaire();
                break;
            case 2 :
                $this->error();
                break;
            case 3 :
                $this->invalid_date();
                break;
        }
        $html = Vue::render($this->content);
        return $html;
    }

    private function afficherFormulaire(){
        $app = Slim::getInstance();
        $valider = $app->urlFor('validerPrestation2');
        $this->content = "<h1>Mes Informations :</h1><div id='formulaire'>";
        $this->content .= <<<END
            <form action='${valider}' method='POST'>
                    <div class='form-group'>
                    <label for='nom'>Nom : </label>
                    <br/>
                    <input type='text' name='nom' id='nom' required/><br>
                     </div>
                    <div class='form-group'>
                    <label for='prenom'>Prénom : </label><br/>
                    <input type='text' name='prenom' id='prenom' required/><br>
                    </div>
                    <div class='form-group'>
                    <label for='mail'>Adresse Email : </label>
                    <br/>
                    <input type='email' name='mail' id='mail' required/><br>
                    </div>
                    <div class='form-group'>
                    <label for='message'>Laissez un petit message ! </label><br>
                    <br/>
                    <textarea name='message' id='message'></textarea><br>
                    </div>
                    <div class='form-group'>
                    <label>Choisissez un moyen de paiement : </label>
                    <input type="radio" id="classique" name="paiement"  checked="checked" value="classique"/><label for="classique" >Classique</label>
                    <input type="radio" id="paiement"  name="paiement" value="cagnotte" /><label for="paiement"  >Cagnotte</label>
                    </div>
                    <div class='form-group'>
                    <label for='password'>Mot de Passe (optionnel) :</label><br>
                    <br/>
                    <input type='password' name='password' id='password'/><br>
                    </div>
                    <input type='submit' value='Valider' class='btn btn-primary'>
                </form>
            </div>
END;
    }

    private function error(){
        $this->content = "<h1>Une erreur s'est produite :</h1><p>$this->message</p>";
    }

    private function invalid_date(){
        $this->content = "<h1>Acces impossible avant le :</h1><p>$this->message</p>";
    }

}