<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/01/2017
 * Time: 22:31
 */

namespace giftbox\vues;


use Illuminate\Database\SQLiteConnection;
use Slim\Slim;

class VueAuthentification
{
    private $content;

    public function render($typeVue){
        switch ($typeVue) {
            case 1 :
                $this->afficherFormulaireInscription();
                break;
            case 2 :
                $this->afficherFormulaireConnexion();
                break;
        }
        $html = Vue::render($this->content);
        return $html;
    }

    private function afficherFormulaireInscription() {
        $app = Slim::getInstance();
        $validerInscription = $app->urlFor('validerInscription');
        $this->content .= "<h1>Inscription :</h1><div class ='panier'>";
        $this->content .= <<<END
        <div>
            <form action='${validerInscription}' method='POST'>
                <label for='identifiant'>Identifiant : </label>
                <input type='text' name='identifiant' id='identifiant' required/><br>

                <label for='password'>Mot de passe : </label>
                <input type='password' name='password' id='password' required/><br>
                <input type='submit' value="S'inscrire" class='btn btn-primary'>
            </form>
        </div>
END;
    }

    private function afficherFormulaireConnexion() {
        $app = Slim::getInstance();
        $validerConnexion = $app->urlFor('validerConnexion');
        $this->content .= "<h1>Connexion :</h1><div class ='panier'>";
        $this->content .= <<<END
        <div>
            <form action='${validerConnexion}' method='POST'>
                <label for='identifiant'>Identifiant : </label>
                <input type='text' name='identifiant' id='identifiant' required/><br>

                <label for='password'>Mot de passe : </label>
                <input type='password' name='password' id='password' required/><br>
                <input type='submit' value="Se connecter" class='btn btn-primary'>
            </form>
        </div>
END;
    }
}