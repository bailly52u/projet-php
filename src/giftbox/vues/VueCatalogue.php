<?php

/**
 * Created by PhpStorm.
 * User: julien
 * Date: 15/12/2016
 * Time: 08:46
 */

namespace giftbox\vues;

use Slim\Slim;

class VueCatalogue
{

    private $content,$listeObjet;

    public function __construct($listeObjet)
    {
        $this->listeObjet=$listeObjet;
    }

    public function render($typeVue)
    {
        switch ($typeVue) {
            case 1:
                $this->content = $this->afficherListePrestation($this->listeObjet);
                break;
            case 2 :
                $this->content = $this->afficherListeCategorie($this->listeObjet);
                break;
            case 3 :
                $this->content = $this->afficherPrestation($this->listeObjet);
                break;
            case 4 :
                $this->content = $this->afficherPrestationsCategorie($this->listeObjet);
                break;
            case 5 :
                $this->content = $this->afficherFormulaireAjoutPrestation();
                break;
        }

        $html = Vue::render($this->content);
        return $html;
    }

    private function afficherListePrestation($prestations)
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $triA = $app->urlFor('triPrix',['sens' => 'ASC']);
        $triD = $app->urlFor('triPrix',['sens' => 'DESC']);
        $formAjoutPrest = $app->urlFor('formAjoutPrestation');

        $content = "<p>Tri: <a href='${triA}'>Croissant</a>-<a href='${triD}'>Décroissant</a></p>";
        foreach ($prestations as $p) {
            $cat = $p->categorie;
            $path= htmlspecialchars($p->img);
            $nom = addslashes($p->nom);
            $desactiverPrest = $app->urlFor('desactiverPrestation',['id_prest' => $p->id]);
            $prestation = $app->urlFor('prestation',['id' => $p->id]);
            $activerPrest =$app->urlFor('activerPrestation',['id_prest' => $p->id]);
            $cate = $app->urlFor('categorie',['id' => $cat->id]);
            $suppr = $app->urlFor('supprPrestation',['id_prest' => $p->id]);
            $content .= <<<END

            <div class="prestation list">
                <h4><a href="${prestation}" >$p->nom</a></h4>
                  $p->prix € - Catégorie : <a href='$cate'>$cat->nom</a><br/>
                 <img src='${root}/img/$path' alt='$nom'>
                 
            
END;

            if(isset($_SESSION['user'])) {
                $content .= "<br>
            <br>
            <form action='${suppr}' method='get'>
                <input type='submit' class='btn btn-primary' value='Supprimer cette prestation'>
            </form>
            <br>";
                if ($p->active == 1) {
                    $content .= "
            <form action='${desactiverPrest}' method='get'>
                <input type='submit' class='btn btn-primary' value='Désactiver cette prestation'>
            </form>";
                } else {
                    $content .= "
            <form action='${activerPrest}' method='get'>
                <input type='submit' class='btn btn-primary' value='Réactiver cette prestation'>
            </form>";
                }
            }
            $content.='</div>';
        }
        return $content;
    }

    private function afficherListeCategorie($listeObjet)
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $content = "";
        foreach ($listeObjet as $c) {
            $cat = $app->urlFor('categorie',['id'=>$c->id]);
            $content .= <<<END

            <div class="categorie list">
                <h4><a href="${cat}">$c->nom</a></h4>
            </div>

END;
        }
        return $content;
    }

    private function afficherPrestation($prestation){
        $app = Slim::getInstance();

        $root = $app->urlFor('root');

        $ajouterPrestationPanier = $app->urlFor('ajouterPrestationPanier');
        $p = $prestation['prestation'];
        $moyenne = $prestation['moyenne'];
        $cat = $p->categorie;
        $notation1 = $app->urlFor('notation',['id'=>$p->id,'note'=>1]);
        $notation2 = $app->urlFor('notation',['id'=>$p->id,'note'=>2]);
        $notation3 = $app->urlFor('notation',['id'=>$p->id,'note'=>3]);
        $notation4 = $app->urlFor('notation',['id'=>$p->id,'note'=>4]);
        $notation5 = $app->urlFor('notation',['id'=>$p->id,'note'=>5]);
        $cate = $app->urlFor('categorie',['id'=>$p->cat_id]);
        $content = <<<END
            <div class="prestation">
                <h4>$p->nom</h4>
                $p->prix € - Catégorie : <a href='$cate'>$cat->nom</a>
                 <p>$p->descr</p>
                 <img src='${root}/img/$p->img' alt='$p->nom'><br>
                 
END;
        $content .= "<div class='note'><div class=\"rating\">
            <a href=${notation5} title=\"Donner 5 étoiles\">☆</a>
            <a href=${notation4} title=\"Donner 4 étoiles\">☆</a>
            <a href=${notation3} title=\"Donner 3 étoiles\">☆</a>
            <a href=${notation2} title=\"Donner 2 étoiles\">☆</a>
            <a href=${notation1} title=\"Donner 1 étoile\">☆</a>
        </div>";
        if($moyenne != 0)
            $content .= "<label>Note moyenne : $moyenne</label></div>";
        else
            $content .= "<label>Aucune note</label></div>";
        $content .= <<<END
                 <form action='${ajouterPrestationPanier}' method='POST'>
                    <input type='hidden' name='id'  value='$p->id'>
                     <label for='qtt'>Quantité : </label>
                    <input type='number' name='qtt' id='qtt' value='1'>
                    <input type='submit' value='Ajouter au Panier' class='btn btn-primary'>
                 </form>
            </div>
END;
        return $content;
    }

    private function afficherPrestationsCategorie($c)
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $content = "<h2>$c->nom</h2>";
        $prestations = $c->prestation;
        foreach ($prestations as $p) {
            $prestat = $app->urlFor('prestation',['id' => $p->id]);
            $content .= <<<END

            <div  class="prestation list">
                
                <h4><a href="${prestat}" >$p->nom</a></h4>
                $p->prix €<br/>
                                 <img src='${root}/img/$p->img' alt='$p->nom'>
            </div>

END;
        }

        return $content;
    }

    private function afficherFormulaireAjoutPrestation(){
        $app = Slim::getInstance();
        $cate = $app->urlFor('categorie');
        $root = $app->urlFor('root');
        $ajoutPresta = $app->urlFor('ajouterPrestation');
    $content = "<h1>Ajout d'une prestation :</h1><div id='formulaire'>";
        $content .= <<<END
            <form action='${ajoutPresta}' method='POST' enctype="multipart/form-data">
                    <div class='form-group'>
                        <label for='nom'>Nom : </label>
                        <br/>
                        <input type='text' name='nom' id='nom' required/><br>
                    </div>
                    <div class='form-group'>
                        <label for='descr'>Description : </label>
                        <br/>
                        <textarea name='descr' id='descr'></textarea><br>
                    </div>
                    <div class='form-group'>
                        <label for='cat'>Catégorie : </label><br/>
                        <select name='cat' id='cat'>
                            <option value='1' selected='selected'>Attention</option>
                            <option value='2'>Activité</option>
                            <option value='3'>Restauration</option>
                            <option value='4'>Hébergement</option>
                        </select>
                    </div>
                    <div class='form-group'>
                        <label for='image'>Image : </label><br/>
                        <input type='file' name='image' id='image' required/>
                    </div>
                    <div class='form-group'>
                        <label for='prix'>Prix : </label><br/>
                        <input type='number' name='prix' id='prix' value='0' min='0' required/><br>
                    </div>
                    <input type='submit' value='Valider' class='btn btn-primary'>
                </form>
            </div>
END;

        return $content;
    }
}