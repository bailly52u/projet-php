<?php
/**
 * Created by PhpStorm.
 * User: Romain KRAFT
 * Date: 20/12/2016
 * Time: 11:11
 */

namespace giftbox\vues;

use giftbox\models\coffret;
use Slim\Slim;

class VueCoffret
{

    private $content, $listeObjet;

    public function __construct($listeObjet)
    {
        $this->listeObjet = $listeObjet;
    }

    public function render($typeVue)
    {
        switch ($typeVue) {
            case 1 :
                $this->afficherMonCoffret();
                break;
            case 2 :
                $this->afficherCoffret();
                break;
            case 3 :
                $this->afficherPaiementCoffret();
                break;
            case 4:
                $this->afficherRecapitulatif();
                break;
            case 5:
                $this->afficherEnvoisCadeau();
                break;
            case 6:
                $this->afficherEnvoisTermine();
                break;
            case 7:
                $this->afficherLogin();
                break;
        }

        $html = Vue::render($this->content);
        return $html;
    }

    private function afficherMonCoffret()
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $ajouterPrestation = $app->urlFor('ajouterPrestationPanier');
        $supprimerPrestation = $app->urlFor('supprimerPrestationPanier');
        $validerPrestation = $app->urlFor('validerPrestation1');
        $this->content = "<h1>Mon Panier :</h1><div class ='panier'>";
        $total = 0;
        if (isset($this->listeObjet)) {
            foreach ($this->listeObjet as $p) {
                $prestation = $p['object'];
                $quantite = $p['quantite'];
                $prix = $prestation->prix * $quantite;
                $total += $prix;
                $this->content .= <<<END
                <div class='prestation'>
                    <div class='pic col-md-3'>
                            <img src='${root}/img/$prestation->img'  alt='$prestation->nom'>
                     </div>
                    <div class='info col-md-6 contenu '>
                             <h3>$prestation->nom</h3>
                             <p>$prestation->descr</p>
                             <p>$prestation->prix €</p>
                     </div>
                    <div class='montant'>
                        <h4>Sous Total : $prix €</h4>
                        <form action='${ajouterPrestation}' method='POST'>
                            <label for='qtt'>Quantité</label>
                            <input type='number' name='qtt' id='qtt' value='$quantite'>
                            <input type='hidden' name='id' value='$prestation->id'>
                            <input type='submit' value='Modifier' class='btn btn-default'>
                        </form>
                        <form action='${supprimerPrestation}' method='POST'>
                            <input type='hidden' name='id' value='$prestation->id'>
                            <input type='submit' value='Supprimer' class='btn btn-primary'>
                        </form>
                    </div>
                </div>
END;
            }
        }
        $this->content .= "<h3 style='text-align: right'>Total : $total €</h3>";
        if (count($this->listeObjet) != 0) {
            $this->content .= '<form action=\''.$validerPrestation.'\' method=\'GET\'> <input type=\'submit\' value=\'Valider\' class=\'btn btn-primary\'>';
        }
        $this->content .= '</div>';
    }


    private function afficherRecapitulatif()
    {
        extract($_POST);
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $paieme = $app->urlFor('paiement');
        $this->content .= "<h1>Recapitulatif :</h1><div class ='panier'>";
        $this->content .= <<<END
        <div class="coordonees">
            <h4>Nom : $nom</h4>
            <h4>Prenom : $prenom</h4>
            <h4>Email : $mail</h4>
            <h4>Message : $message</h4>
            <h4>Paiement : $paiement</h4>
        </div>
END;
        $total = 0;
        $this->content .= "<div class ='panier'>";
        if (isset($this->listeObjet)) {
            foreach ($this->listeObjet as $p) {
                $prestation = $p['object'];
                $quantite = $p['quantite'];
                $prix = $prestation->prix * $quantite;
                $total += $prix;
                $this->content .= <<<END
            <div class='prestation'>
                     <div class='col-md-3'>
                        <img src='${root}/img/$prestation->img'  alt='$prestation->nom'>
                     </div>
                <div class='info col-md-6 contenu '>
                         <h3>$prestation->nom</h3>
                         <p>$prestation->descr</p>
                </div>
                <div class='montant'>
                    <h3>Quantité :</h3><p>$quantite</p>
                </div>
            </div>
END;
            }
        }
        $this->content .= "<h3 style='text-align: right'>Total : $total €</h3>";
        $this->content .= '<form action=\''.$paieme.'\' method=\'GET\'> <input type=\'submit\' value=\'Confirmer\' class=\'btn btn-primary\'>';
        $this->content .= '</div>';
    }


    private function afficherCoffret()
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $paieme = $app->urlFor('paiement');

        //On vérifie si le coffret existe
        if (isset($this->listeObjet)) {
            $status = $this->listeObjet->status;
            switch ($status) {
                case 0 : 
                    $status = "En cours de paiement";
                    break;
                case 1 :
                    $status = "En cours d'ouverture";
                    break;
                case 2 :
                    $status = "Ouvert";
                    break;
                case 3 :
                    $status = "Envoyé";
                    break;
            }
            $url = 'de la part de ' . $this->listeObjet->mail_crea;

            $url .= ($this->listeObjet->mode_paiement == 0) ? ' (Paiement Classique)' : ' (Paiement Cagnotte)';
            $this->content = "<h1>Coffret $url :</h1>
            <p>Status du cadeau : $status</p>
            <div class ='panier'>";

            //On affiche toutes les prestations du coffret
            foreach ($this->listeObjet->prestation as $prestation) {
                $quant = $prestation->pivot->quantite;
                $this->content .= <<<END
                <div class='prestation'>
                     <div class='col-md-3'>
                        <img src='${root}/img/$prestation->img'  alt='$prestation->nom'>
                     </div>
                <div class='info col-md-6 contenu '>
                         <h3>$prestation->nom</h3>
                         <p>$prestation->descr</p>
                </div>
                <div class='montant'>
                    <h3>Quantité :</h3><p>$quant</p>
                </div>
            </div>
        
END;
            }
            if ($this->listeObjet->mode_paiement == 1 && $this->listeObjet->somme_atteinte>=$this->listeObjet->prix_total && $this->listeObjet->status==0) {
                $this->listeObjet->status=3;
                $this->listeObjet->paye=1;

                $this->listeObjet->url_cagnotte="";
                $this->listeObjet->save();
                $url_gestion=$this->listeObjet->url_gestion;
                $id=$this->listeObjet->coffret_id;
                $app = \Slim\Slim::getInstance();
                $urlEnvoisCadeau=$app->urlFor('envoisCadeauCagnotte');
                $this->content.="<br><form action='$urlEnvoisCadeau' method='POST'>
                    <label for='mail'>Adresse Email : </label>
                    <input type='email' name='mail' id='mail' required/><br>
                    <label for=\"date\"  >Date d'ouverture</label>
                    <input type=\"date\" id='date' name='date_ouverture' placeholder='DD-MM-YYYY' />
                    (si ouverture a une date)
                    <input type='hidden' name='coffret_id' value='$id'>
                    <input type='submit' value='Valider' class='btn btn-primary'>
                    <br>";
            }
            $this->content .= "</div>";
        } else {
            $this->content .= '<h1>Erreur:</h1><h3>Coffret Introuvable</h3>';
        }
    }

    private function afficherPaiementCoffret()
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $envoisCadeau = $app->urlFor('envoisCadeau');
        $this->content = "<h1>Paiement classique de mon Coffret :</h1><div class ='panier'>";
        if (isset($this->listeObjet)) {
            $total = 0;
            foreach ($this->listeObjet as $p) {
                $prestation = $p['object'];
                $quantite = $p['quantite'];
                $prix = $prestation->prix * $quantite;
                $total += $prix;
            }

            $this->content .= <<<END
                <div class='montant'>
                    <h4>Cout total : $total €</h4>
                    <form action='${envoisCadeau}' method='POST'>
                    
                        <label for='numCarte'>Numéro de carte : </label>
                        <input type='number' name='numCarte' id='numCarte' placeholder='0000-0000-0000-0000' required>
                        <br>
                        <label for='date'>Date de validité : </label>
                        <select name="mois" id='date'>
                            <option>01 - Janvier</option>
                            <option>02 - Février</option>
                            <option>03 - Mars</option>
                            <option>04 - Avril</option>
                            <option>05 - Mai</option>
                            <option>06 - Juin</option>
                            <option>07 - Juillet</option>
                            <option>08 - Août</option>
                            <option>09 - Septembre</option>
                            <option>10 - Octobre</option>
                            <option>11 - Novembre</option>
                            <option>12 - Décembre</option>
                        </select>
                        <select name="annee">
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                            <option>2020</option>
                            <option>2021</option>
                            <option>2022</option>
                            <option>2023</option>
                            <option>2024</option>
                            <option>2025</option>
                            <option>2026</option>
                            <option>2027</option>
                            <option>2028</option>
                        </select>
                        <br>
                        <label for='cryp'>Cryptogramme : </label>
                        <input type='number' value='000' name='cryp' required>
                        <br>
                        <input type='submit' value='Valider'>
                    </form>
                </div>
END;

        }
    }

    private function afficherEnvoisCadeau()
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');
        $cadeauEnvoye = $app->urlFor('cadeauEnvoye');
        $this->content = "<h1>Envois cadeau :</h1>";
        if (isset($this->listeObjet)) {
            $this->content .= <<<END
                <form action='${cadeauEnvoye}' method='POST'>
                    <label for='mail'>Adresse Email : </label>
                    <input type='email' name='mail' id='mail' required/><br>

                    <label>Choisissez un moyen d'ouverture du cadeau' : </label>
                    <input type="radio" id="1by1" name="typeOuverture"  checked="checked" value="1"/><label for="1by1" >Un par un</label>
                    <input type="radio" id="allDate"  name="typeOuverture" value="2" /><label for="allDate"  >Tout a une date</label><br>
END;
            if($_SESSION['indiceFormulaire']!= 4) {
                $this->content .= <<<END
                    <label for="date"  >Date d'ouverture</label>
                    <input type="date" id='date' name='date_ouverture' placeholder='DD-MM-YYYY' />
                    (si ouverture a une date)
                    <br>
END;
            }
            $this->content .= <<<END
                    <input type='submit' value='Valider' class='btn btn-primary'>
                </form>
            </div>
END;

        }
    }

    private function afficherEnvoisTermine()
    {
        $app = Slim::getInstance();
        $root = $app->urlFor('root');

        $cadeauEnvoye = $app->urlFor('cadeauEnvoye');
        if($_SESSION['indiceFormulaire']!=4) {
            $this->content = "<h1>Envois du cadeau terminé</h1>";
        }else{
            $this->content = "<h1>Création de la cagnotte terminé</h1>";
        }
        $co = $this->listeObjet;
        $gestion = $app->urlFor('gestion',['url' => $co->url_gestion]);
        $cagnotte = $app->urlFor('cagnotte',['url_cagnotte' => $co->url_cagnotte]);
        if ($co->mode_paiement==1 && $co->status!=3) {
            $lienCagnotte = "<br/><a href=${cagnotte}>Url de la cagnotte</a>";
        } else {
            $lienCagnotte = "";
        }
        $this->content .= <<<END
            <a href=${gestion}>Gestion du coffret</a>
            $lienCagnotte
            
END;
    }

    private function afficherLogin()
    {
        $this->content = <<<END
        <h1>Authentification</h1>
        <form method='POST'>
        <label for='password'>Mot de passe :</label><br/>
        <input type='password' id='password' name='password'>
        <input type='submit' value='Connexion'>
        </form>
END;

    }
}