<?php
/**
 * Created by PhpStorm.
 * User: Romain KRAFT
 * Date: 20/12/2016
 * Time: 11:22
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class coffret extends Model
{
    public $timestamps = false;
    protected $table = 'coffret';
    protected $primaryKey = "coffret_id";

    public function prestation()
    {
        //permet de récuperer les prestations associées à un coffret
        return $this->belongsToMany('giftbox\models\prestation')->withPivot('quantite');
    }
}