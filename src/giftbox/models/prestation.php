<?php
namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: alexi
 * Date: 05/12/2016
 * Time: 11:22
 */
class prestation extends Model
{
    protected $table = 'prestation';
    protected $primaryKey = "id";
    public $timestamps = false;

    public function categorie(){
        return $this->belongsTo('giftbox\models\categorie','cat_id');
    }

    public function coffret()
    {
        return $this->belongsToMany('giftbox\models\coffret');

    }

    public function note(){
        //permet de récupérer les notes associées à une prestation
        return $this->belongsToMany('giftbox\models\note')->withPivot('quantite');
    }
}