<?php
namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: alexi
 * Date: 05/12/2016
 * Time: 11:24
 */
class categorie extends Model
{
    protected $table = 'categorie';
    protected $primaryKey = "id";
    public $timestamps = false;

    public function prestation(){
        return $this->hasMany('giftbox\models\prestation','cat_id');
    }
}