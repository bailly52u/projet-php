<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 10/01/2017
 * Time: 09:50
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class utilisateur extends Model
{
    protected $table = 'utilisateur';
    protected $primaryKey = "userName";
    public $timestamps = false;
}