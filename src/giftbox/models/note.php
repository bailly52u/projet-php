<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 10/01/2017
 * Time: 22:29
 */

namespace giftbox\models;


use Illuminate\Database\Eloquent\Model;

class note extends Model
{
    protected $table = 'note';
    protected $primaryKey = 'idnote';
    protected $timestamp = false;

    public function prestation(){
        return $this->belongsToMany('giftbox\models\prestation');
    }
}