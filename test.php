<?php
use giftbox\models\categorie as Categorie;
use giftbox\models\prestation as Prestation;
use Illuminate\Database\Capsule\Manager as DB;


require("vendor/autoload.php");
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$cat = Categorie::get();
foreach ($cat as $cats) {
    echo '<p>' . $cats->id . ' ' . $cats->nom . '</p>';
    $prestations = Prestation::select()->where('cat_id', '=', $cats->id)->get();
    foreach ($prestations as $prestation) {
        echo '<p>    ' . $prestation->nom . ' ' . $prestation->descr . '</p>';
        echo '<img src=\'img/' . $prestation->img . '\' style=\'width:300px;height:auto;\'/>';
    }
}

if (isset($_GET['id'])) {
    $cats = Categorie::where('id', '=', $_GET['id'])->first();
    echo '</p>' . $cats->nom . '</p>';
}

/*
$p = new Prestation();
$p->nom = 'Espiraclette';
$p->save();
$p->cat_id = 3;
$p->save();
*/

$prestations = Prestation::get();
foreach ($prestations as $prestation) {
    $cat = $prestation->categorie()->first();
    echo '<p>' . $prestation->nom . ' ' . $cat->nom . '</p>';
}


