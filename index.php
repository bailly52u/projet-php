<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/12/2016
 * Time: 17:07
 */

use giftbox\controleurs\controllerCoffret;
use giftbox\controleurs\controllerUtilisateur;
use \giftbox\controleurs\catalogue;
use giftbox\vues\VueAccueil;
use Illuminate\Database\Capsule\Manager as DB;

require 'vendor/autoload.php';

session_start();

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/', function () {
    $accueil = new VueAccueil();
    $accueil->render();
})->name('root');

$app->get('/listePrestations', function () {
    $catalogue = new catalogue();
    if (!isset($_SESSION['user']))
        $catalogue->listerPrestations();
    else
        $catalogue->listerPrestationsAdmin();
})->name('lPrestation');

$app->get('/prestation/:id', function ($id) {
    $catalogue = new catalogue();
    $catalogue->donnerPrestation($id);
})->name('prestation');

$app->get('/listeCategories', function () {
    $catalogue = new catalogue();
    $catalogue->listerCategories();
})->name('lCategorie');

$app->get('/categorie/:id', function ($id) {
    $catalogue = new catalogue();
    $catalogue->donnerPrestationsCategorie($id);
})->name('categorie');

$app->get('/triPrix/:sens', function ($sens) {
    $catalogue = new catalogue();
    $catalogue->trierPrix($sens);
})->name('triPrix');

$app->get('/monCoffret/', function () {
    $coffret = new controllerCoffret();
    $coffret->afficherMonCoffret();
})->name('coffret');

$app->post('/ajouterPrestationPanier/', function () use ($app) {
    $id = $app->request()->post('id');
    $qtt = $app->request()->post('qtt');
    $coffret = new controllerCoffret();
    $return = $coffret->ajouterPrestation($id, $qtt);
    if ($return != 1) {
        $app->response->redirect($app->urlFor('coffret'));
    }
})->name('ajouterPrestationPanier');

$app->post('/supprimerPrestationPanier/', function () use ($app) {
    $id = $app->request()->post('id');
    $coffret = new controllerCoffret();
    $coffret->supprimerPrestation($id);
    $app->response->redirect($app->urlFor('coffret'));
})->name('supprimerPrestationPanier');

$app->get('/validerPrestation1', function () {
    $coffret = new controllerCoffret();
    $coffret->verifierCoffret();
})->name('validerPrestation1');

$app->post('/validerPrestation2/', function () use ($app) {
    $infos['nom'] = $app->request()->post('nom');
    $infos['prenom'] = $app->request()->post('prenom');
    $infos['mail'] = $app->request()->post('mail');
    $infos['msg'] = $app->request()->post('message');
    $infos['paiement'] = $app->request()->post('paiement');
    $_SESSION['infos'] = $infos;
    $coffret = new controllerCoffret();
    $coffret->validerCoffret();
})->name('validerPrestation2');

$app->get('/coffret_gestion/:url', function ($url) {
    $coffret = new controllerCoffret();
    $coffret->suiviCoffret($url);
})->name('gestion');

$app->post('/coffret_gestion/:url', function ($url) {
    $coffret = new controllerCoffret();
    $coffret->login($url);
})->name('gestion_post');

$app->get('/paiement/', function () {
    $coffret = new controllerCoffret();
    $coffret->afficherPaiementCoffret();
})->name('paiement');

$app->post('/envoisCadeau/', function () {
    $coffret = new controllerCoffret();
    $coffret->preparerEnvois();
})->name('envoisCadeau');

$app->post('/cadeauEnvoye/', function () {
    $coffret = new controllerCoffret();
    $coffret->genererURL();
})->name('cadeauEnvoye');

$app->get('/cadeau/:url_cadeau', function ($url_cadeau) {
    $cadeau = new controllerCoffret();
    $cadeau->afficherCadeaux($url_cadeau);
})->name('cadeau');

$app->get('/cagnotte/:url_cagnotte', function ($url_cagnotte) {
    $coffret = new controllerCoffret();
    $coffret->afficherCagnotte($url_cagnotte);
})->name('cagnotte');

$app->post('/enregistrerParticipation/', function () {
    $coffret = new controllerCoffret();
    $coffret->enregistrerParticipation();
})->name('enregistrerParticipation');

$app->post('/envoisCadeauCagnotte/', function () {
    $coffret = new controllerCoffret();
    $coffret->envoisCadeauCagnotte();
})->name('envoisCadeauCagnotte');

//gestion des notes :

$app->get('/notation/:id/:note', function ($id, $note) use ($app) {
    $catalogue = new catalogue();
    $catalogue->noterPrestation($id, $note);
    $app->response->redirect($app->urlFor('lPrestation'));
})->name('notation');

//gestion des utilisateurs :
$app->get('/inscription', function () {
    $user = new controllerUtilisateur();
    $user->envoyerFormulaireInscription();
})->name('inscription');

$app->post('/validerInscription', function () use ($app) {
    $infos['id'] = $app->request()->post('identifiant');
    $infos['passwd'] = $app->request()->post('password');
    $user = new controllerUtilisateur();
    $user->verifierFormulaireInscription($infos);
})->name('validerInscription');

$app->get('/connexion', function () {
    $user = new controllerUtilisateur();
    $user->envoyerFormulaireConnexion();
})->name('connexion');

$app->post('/validerConnexion', function () use ($app) {
    $infos['id'] = $app->request()->post('identifiant');
    $infos['passwd'] = $app->request()->post('password');
    $user = new controllerUtilisateur();
    $user->verifierFormulaireConnexion($infos);
})->name('validerConnexion');

$app->get('/deconnexion', function () use ($app) {
    $user = new controllerUtilisateur();
    $user->deconnecterUtilisateur();
    $app->response->redirect($app->urlFor('lPrestation'));
})->name('deconnexion');

//gestion des prestation :

$app->get('/formAjoutPrestation', function () {
    $catalogue = new catalogue();
    $catalogue->formAjoutPrestation();
})->name('formAjoutPrestation');

$app->get('/supprPrestation/:id_prest', function ($id_prest) use ($app) {
    $catalogue = new catalogue();
    $retour = $catalogue->supprPrestation($id_prest);
    if ($retour == 1) {
        $app->response->redirect($app->urlFor('lPrestation'));
    }
})->name('supprPrestation');

$app->get('/desactiverPrestation/:id_prest', function ($id_prest) use ($app) {
    $catalogue = new catalogue();
    $catalogue->desactiverPrestation($id_prest);
    $app->response->redirect($app->urlFor('lPrestation'));
})->name('desactiverPrestation');

$app->get('/activerPrestation/:id_prest', function ($id_prest) use ($app) {
    $catalogue = new catalogue();
    $catalogue->activerPrestation($id_prest);
    $app->response->redirect($app->urlFor('lPrestation'));
})->name('activerPrestation');

$app->post('/ajouterPrestation', function () use ($app) {
    $infos['nom'] = $app->request()->post('nom');
    $infos['descr'] = $app->request()->post('descr');
    $infos['cat'] = $app->request()->post('cat');
    $infos['prix'] = $app->request()->post('prix');
    $catalogue = new catalogue();
    $succes = $catalogue->ajouterPrestation($infos);
    if ($succes)
        $app->response->redirect($app->urlFor('lPrestation'));
})->name('ajouterPrestation');

$app->run();